﻿-- Ejercicio 1. Visualizar el nº de empleados de cada departamento. Utilizar GROUP BY para agrupar por departamento.

USE emple_depart;

SELECT DISTINCT e.dept_no,COUNT(*)nºempleados 
FROM emple e 
GROUP BY e.dept_no;

-- Ejercicio 2. Visualizar los departamentos con más de 5 empleados. Utilizar GROUP BY para agrupar por departamento y HAVING para establecer la condición sobre los grupos.

SELECT DISTINCT e.dept_no,COUNT(*)nempleados 
FROM emple e 
GROUP BY e.dept_no 
HAVING nempleados>5;

-- Ejercicio 3. Hallar la media de los salarios de cada departamento(utilizar la función avg y GROUP BY).

SELECT DISTINCT e.dept_no, AVG(e.salario) 
FROM  emple e
GROUP BY e.dept_no;

-- Ejercicio 4. Visualizar el nombre de los empleados vendedores del departamento 'VENTAS' (Nombre del departamento='VENTAS', oficio='VENDEDOR')

SELECT DISTINCT d.dept_no 
FROM depart d 
WHERE d.dnombre='VENTAS';

SELECT DISTINCT e.apellido 
FROM emple e 
WHERE e.oficio='VENDEDOR'; 

-- Ejercicio 5. Visualizar el nº de vendedores del departamento 'VENTAS' (utilizar la función COUNT sobre la consulta anterior).

SELECT DISTINCT e.apellido,COUNT(*) vendedores 
FROM emple e WHERE e.oficio='VENDEDOR'
AND e.dept_no=(SELECT DISTINCT d.dept_no 
FROM depart d WHERE d.dnombre='VENTAS');

-- Ejercicio 6. Visualizar los oficios de los empleados del departamento 'VENTAS'

SELECT DISTINCT d.dept_no FROM depart d WHERE d.dnombre='VENTAS';

SELECT DISTINCT e.oficio FROM emple e WHERE e.dept_no=( SELECT DISTINCT d.dept_no FROM depart d WHERE d.dnombre='VENTAS');

-- Ejercicio 7. A partir de la tabla EMPLE, visualizar el nº de empleados de cada departamento cuyo oficio sea 'EMPLEADO' (utilizar GROUP BY para agrupar por departamento.En la cláusula WHERE habrá que indicar que el oficio es 'EMPLEADO')

SELECT DISTINCT e.dept_no,COUNT(*)numempleados 
FROM emple e 
WHERE e.oficio='EMPLEADO' GROUP BY e.dept_no;

-- Ejercicio 8. Visualizar el departamento con más empleados.

-- Consulta 1 que llamamos C1.

SELECT DISTINCT e.dept_no, COUNT(*) empleados FROM emple e;

-- Consulta 2 que llamamos C2.

-- C2
      
SELECT DISTINCT MAX(empleados) maxempleados
FROM (SELECT DISTINCT e.dept_no, COUNT(*) empleados FROM emple e)C1;

-- Esta es la final con las consultas C1 y C2.

SELECT DISTINCT C1.dept_no
FROM (SELECT DISTINCT e.dept_no, COUNT(*) empleados FROM emple e) C1
JOIN (SELECT DISTINCT MAX(empleados) maxempleados 
FROM (SELECT DISTINCT e.dept_no, COUNT(*) empleados FROM emple e)C1)C2 ON C1.dept_no;

-- Ejercicio 9. Mostar los departamentos cuya suma de salarios sea mayor que la media de salarios de todos los empleados.

SELECT DISTINCT AVG(e.salario)mediasalarios FROM emple e;

-- Ejercicio 10. Para cada oficio obtener la suma de salarios.

SELECT DISTINCT e.oficio,SUM(e.salario) sumasalarios FROM emple e;

-- Ejercicio 11. Visualizar la suma de salarios de cada oficio del departamento ʻVENTASʼ.

-- Consulta 1 c1

SELECT DISTINCT d.dept_no FROM depart d WHERE d.dnombre='VENTAS';

 -- Final.

SELECT DISTINCT e.oficio,SUM(e.salario) sumasalarios FROM emple e WHERE e.dept_no=(SELECT DISTINCT d.dept_no FROM depart d WHERE d.dnombre='VENTAS');

-- Ejercicio 12. Visualizar el número de departamento que tenga más empleados cuyo oficio sea empleado.

-- Consulta primera c1

 SELECT e.dept_no, COUNT(*) empleados FROM emple e WHERE e.oficio='EMPLEADOS' ORDER BY empleados ;

 -- Consulta segunda c2

 SELECT c1.dept_no, MAX(c1.empleados) maximo FROM ( SELECT e.dept_no, COUNT(*) empleados FROM emple e WHERE e.oficio='EMPLEADOS' ORDER BY empleados )c1;

 -- Consulta final

 SELECT c1.dept_no  FROM ( SELECT e.dept_no, COUNT(*) empleados FROM emple e WHERE e.oficio='EMPLEADOS' ORDER BY empleados )c1 JOIN
   (SELECT c1.dept_no, MAX(c1.empleados) maximo FROM ( SELECT e.dept_no, COUNT(*) empleados FROM emple e WHERE e.oficio='EMPLEADOS'ORDER BY empleados )c1)c2 ON
                 c1.empleados=c2.maximo;

-- Ejercicio 13

-- Mostrar el número de oficios distintos de cada departamento.

SELECT e.dept_no, COUNT(DISTINCT e.oficio)oficios FROM emple e; 

-- Ejercicio 14. 

-- Mostrar los departamentos que tengan más de dos personas trabajando en la misma profesión.

SELECT e.dept_no,e.oficio,COUNT(*)numeroempleado FROM emple e WHERE e.dept_no>2;

-- Ejercicio 15. 

-- Dada la tabla HERRAMIENTAS, visualizar por cada estantería la suma de las unidades.

  USE hospitales;

SELECT DISTINCT h.estanteria,SUM(h.unidades) unidades FROM herramientas h;

-- Ejercicio 16.

-- Visualizar la estantería con más unidades de la tabla HERRAMIENTAS. (con totales y sin totales)

-- c1      
       
SELECT h.estanteria,SUM(h.unidades) unidades FROM herramientas h;

-- c2

SELECT MAX(c1.unidades) maximo FROM ( SELECT h.estanteria,SUM(h.unidades) unidades FROM herramientas h)c1;

-- Ejercicio 17.

-- Mostrar el número de médicos que pertenecen a cada hospital, ordenado por número descendente de hospital.

SELECT DISTINCT m.cod_hospital des, COUNT(*) numeromedicos FROM medicos m; 

-- Ejercicio 18.

-- Realizar una consulta en la que se muestre por cada hospital el nombre de las especialidades que tiene.

SELECT m.especialidad FROM medicos m;

-- Ejercicio 19.

-- Realizar una consulta en la que aparezca por cada hospital y en cada especialidad el número de médicos (tendrás que partir
-- de la consulta anterior y utilizar GROUP BY).

SELECT DISTINCT m.especialidad,COUNT(*)nummedicos FROM medicos m;

-- Ejercicio 20.

-- Obtener por cada hospital el número de empleados.

SELECT DISTINCT p.cod_hospital,COUNT(*) empleados FROM personas p;

-- Ejercicio 21.

-- Obtener por cada especialidad el número de trabajadores.

SELECT DISTINCT m.especialidad,COUNT(*)empleados FROM medicos m;

-- Ejercicio 22.

--  Visualizar la especialidad que tenga más médicos.

-- Consulta 1 C1

SELECT m.especialidad,COUNT(*) empleados FROM medicos m GROUP BY m.especialidad;

-- Consulta 2 C2

SELECT MAX(empleados) maximo FROM (SELECT m.especialidad,COUNT(*) empleados FROM medicos m GROUP BY m.especialidad)c1;


-- final 

    SELECT  c1.especialidad FROM ( SELECT m.especialidad,COUNT(*) empleados FROM medicos m GROUP BY m.especialidad)c1 JOIN
      (SELECT MAX(empleados) maximo FROM (SELECT m.especialidad,COUNT(*) empleados FROM medicos m GROUP BY m.especialidad)c1) c2
      ON c1.empleados=c2.maximo;

-- Ejercicio 23.

-- ¿Cuál es el nombre del hospital que tiene mayor número de plazas?

-- c1
         
   SELECT DISTINCT MAX(h.num_plazas) maximo FROM hospitales h;

-- final

   SELECT DISTINCT h.nombre FROM hospitales h WHERE h.num_plazas=(SELECT DISTINCT MAX(h.num_plazas) maximo FROM hospitales h);


-- Ejercicio 24.

-- Visualizar las diferentes estanterías de la tabla HERRAMIENTAS ordenados descendentemente por estantería.

   SELECT DISTINCT h.estanteria FROM herramientas h ORDER BY h.estanteria DESC;

-- Ejercicio 25.

-- Averiguar cuántas unidades tiene cada estantería.

   SELECT h.estanteria,SUM(h.unidades) unidades FROM herramientas h; 

-- Ejercicio 26.

-- Visualizar las estanterías que tengan más de 15 unidades

   SELECT h.estanteria, SUM(h.unidades) mayor FROM herramientas h WHERE h.unidades>15; 

-- Ejercicio 27.

-- ¿Cuál es la estantería que tiene más unidades?

   -- Consulta1 c1
         SELECT h.estanteria, COUNT(h.unidades) unidades FROM herramientas h GROUP BY h.estanteria;

   -- Consulta2 c2

         SELECT MAX(unidades) maximo  FROM ( SELECT h.estanteria, COUNT(h.unidades) unidades FROM herramientas h GROUP BY h.estanteria)c1;
 
    -- Consulta final

        SELECT c1.estanteria FROM ( SELECT h.estanteria, COUNT(h.unidades) unidades FROM herramientas h GROUP BY h.estanteria)c1 JOIN
          (SELECT MAX(unidades) maximo  FROM ( SELECT h.estanteria, COUNT(h.unidades) unidades FROM herramientas h GROUP BY h.estanteria)c1)c2 
          ON c1.unidades=c2.maximo;

-- Ejercicio 28.

-- A partir de las tablas EMPLE y DEPART mostrar los datos del departamento que no tiene ningún empleado.







